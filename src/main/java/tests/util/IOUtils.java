package tests.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by alvarot on 02-09-2019.
 */
public class IOUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(IOUtils.class);

    private IOUtils() {    	
    }

    /**
     * Read a CSV file, spliting the values using \";\"
     *
     * @param pathToCSVFIle
     * @return
     */
    public static ArrayList<String> readTestCasesFromCSVFile(String pathToCSVFIle) {
        LOGGER.info("-- readTestCasesFromCSVFile method started --");
        LOGGER.info("CSV file to read: {}", pathToCSVFIle);
        BufferedReader reader = null;
        String errorMsg = null;
        ArrayList<String> casesList = new ArrayList<>();
        try {
            String line;
            Path path = Paths.get(pathToCSVFIle);
            reader = Files.newBufferedReader(path, Charset.forName("UTF-8"));
            while ((line = reader.readLine()) != null) {
                String[] cases;
                Arrays.stream(cases = (line.split(";"))).filter(s -> !s.isEmpty() || !"".equalsIgnoreCase(s));
                casesList.addAll(Arrays.asList(cases));
            }
        } catch (FileNotFoundException e) {
            errorMsg = String.format("The file %s was not found", pathToCSVFIle);
            LOGGER.error(errorMsg, e);
        } catch (IOException e) {
            errorMsg = String.format("Error attempting to read the file located in %s", pathToCSVFIle);
            LOGGER.error(errorMsg, e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    LOGGER.error("Error attempting to close the file located in {}", pathToCSVFIle, e);
                }
            }
        }
        if (errorMsg != null) {
            throw new Error(errorMsg);
        }
        LOGGER.info("-- readTestCasesFromCSVFile method finished --");
        return casesList;        
        
    }

    public static boolean deleteDirectoryRecursively(String path) throws IOException {
        Path tempDirectory = Files.createTempDirectory("tmpDir");
        Path pathToDelete = tempDirectory.resolve(path);

        Files.walk(pathToDelete)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);

        return !Files.exists(pathToDelete);
    }

}
