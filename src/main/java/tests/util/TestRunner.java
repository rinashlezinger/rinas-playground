package tests.util;

import org.junit.jupiter.api.Test;
import tests.myGames.FirstTest;
import tests.myGames.IframeTest;
import tests.myGames.SecondTest;

import java.util.HashMap;
import java.util.Map;
import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;


public class TestRunner {

    public static Map<String,Class>classesOfTest=new HashMap<>();

    public static void setMap(){
        classesOfTest.put("IframeTest", IframeTest.class);
        classesOfTest.put("SecondTest", SecondTest.class);
    }

    public static void runSuite(String []testCasesIDs){

        //This will take each of test case defined in CSV file as an String array
        //ArrayList<String> testCasesIDs= IOUtils.readTestCasesFromCSVFile(csvFilePath);
//        ArrayList<String> testCasesIDs=new ArrayList<>();
//        testCasesIDs.add("FirstTest");


        //For each one of the test cases ID finded, will be instantiate its own Test Case class
        //loadTestCases(testCasesIDs);
        setMap();

        for (String testCase : testCasesIDs) {
            runTest(classesOfTest.get(testCase));
        }
    }

    public static void runTest(Class classOfTest) {

        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
                .selectors(DiscoverySelectors.selectClass(classOfTest)).build();
        Launcher launcher = LauncherFactory.create();
        launcher.execute(request);
    }

}
