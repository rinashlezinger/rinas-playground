package tests.myGames;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import tests.util.TestRunner;


public class DemoSuite extends TestRunner {


    @BeforeAll
    public static void init(){
        System.out.println("DemoSuite.BeforeAll");
    }

    @Test
    public void function(){
       String [] testCasesIDs= {"IframeTest","SecondTest"};
        runSuite(testCasesIDs);
    }

   @AfterEach
    public void quitDriver(){
        System.out.println("DemoSuite.AfterEach");
   }


}
