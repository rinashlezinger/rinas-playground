package tests.myGames;

import org.junit.jupiter.api.*;

import java.util.ArrayList;


public class FirstTest {

    final ArrayList<String>tags=new ArrayList<>();

    @BeforeEach
    public  void each(TestInfo info){
        tags.addAll(info.getTags());
        System.out.println("FirstTest.BeforeEach");
    }

    @Tag("method1")
    @Test
    public void first() {
        System.out.println("FirstTest.test1");
    }

    @Tag("method2")
    @Test
    public void first2() {
        System.out.println("FirstTest2.test2");
    }
}
