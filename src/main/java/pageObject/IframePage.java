package pageObject;

import common.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class IframePage {

    WebDriver driver= Browser.getDriver();
    WebElement switchTo=driver.findElement(By.xpath("//a[text()='SwitchTo']"));
    WebElement frames=driver.findElement(By.xpath("//a[text()='Frames']"));
    //WebElement divInIframe=driver.findElement(By.cssSelector("div[class='col-xs-6 col-xs-offset-5']"));

    public void doActions(){

        System.out.println(driver.getWindowHandle());
        switchTo.click();
        frames.click();
        driver.switchTo().frame("SingleFrame");
        System.out.println(driver.getWindowHandle());
        //divInIframe.getText();
    }


}
