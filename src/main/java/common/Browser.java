package common;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Browser {

    private static WebDriver driver;

        public static WebDriver getDriver() {
            if (driver == null) {
                WebDriverManager.chromedriver().setup();
                driver=new ChromeDriver();
                driver.manage().window().maximize();
                driver.get("http://demo.automationtesting.in/Frames.html");
            }
            return driver;
        }

        public static void quitDriver(){
            driver.quit();
        }
}

