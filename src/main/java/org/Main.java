package org;

import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import tests.util.IOUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    private static List<Object> testCasesList = new ArrayList<>();

    public static void main(String[]args){

        String csvFilePath = "C:/Users/Rina/OneDrive - Surecomp/TestCases/TesrCases.csv";
        //This will take each of test case defined in CSV file as an String array
        ArrayList<String> testCasesIDs = IOUtils.readTestCasesFromCSVFile(csvFilePath);
        //For each one of the test cases ID finded, will be instantiate its own Test Case class
        loadTestCases(testCasesIDs);

        for (Object testCase : testCasesList) {
            runTest(testCase.getClass());
            System.out.println("***");
        }
    }


    private static void runTest(Class className) {
        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
                    .selectors(DiscoverySelectors.selectClass(className)).build();
        Launcher launcher = LauncherFactory.create();
        launcher.execute(request);
    }


    private static void loadTestCases(ArrayList<String> testCases) {
        testCases.stream()
                .filter(testCaseID ->!testCaseID.equals(null) && !testCaseID.contains("TestsCases"))
                .forEach(testCaseID -> {
            Class<?> tstClass ; // convert string classname to class
            Object tstClassObj;
            try {
                String path="tests.myGames."+testCaseID;
                tstClass = Class.forName(path);
                tstClassObj = tstClass.newInstance();// invoke empty constructor
            } catch (Exception e) {
                System.out.println("Class"+testCaseID+" not found" +e);
                return;
            }
            testCasesList.add(tstClassObj);
        });
    }
}
